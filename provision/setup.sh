#!/bin/bash

export DEBIAN_FRONTEND=noninteractive

echo "Provisioning virtual machine..."

locale-gen ru_RU.UTF-8

echo "Updating repositories"
apt-get install python-software-properties build-essential -y > /dev/null
add-apt-repository ppa:ondrej/php5 -y > /dev/null

add-apt-repository "deb https://apt.postgresql.org/pub/repos/apt/ trusty-pgdg main" -y
wget --quiet -O - https://postgresql.org/media/keys/ACCC4CF8.asc | apt-key add - 
apt-get update > /dev/null

# Git
echo "Installing Git"
apt-get install git mc -y > /dev/null

# Nginx
echo "Installing Nginx"
apt-get install nginx -y > /dev/null

# PHP
echo "Installing PHP"
apt-get install php5-common php5-dev php5-cli php5-fpm -y > /dev/null

echo "Installing PHP extensions"
apt-get install curl php5-curl php5-gd php5-mcrypt php5-mysql php5-pgsql php5-intl libicu-dev -y > /dev/null 
apt-get install php5-xdebug -y > /dev/null 

echo "Installing composer"
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer
sudo -u vagrant composer global require "fxp/composer-asset-plugin:*"

# MySQL 
echo "Preparing MySQL"
apt-get install debconf-utils -y > /dev/null
debconf-set-selections <<< "mysql-server mysql-server/root_password password 1234"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password 1234"

echo "Installing MySQL"
apt-get install mysql-server -y > /dev/null

echo "Installing PosgreeSql"
apt-get install postgresql-9.4 -y > /dev/null

echo "Configuring PosgreeSql"
echo "CREATE USER pguser WITH password 'pguser';" | sudo -u postgres psql
echo "GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO pguser;" | sudo -u postgres psql

# Nginx Configuration
echo "Configuring Nginx & php-fpm"
cp /var/www/vagrant_base/provision/config/etc/nginx/nginx.conf /etc/nginx/nginx.conf
cp /var/www/vagrant_base/provision/config/etc/php5/fpm/php.ini /etc/php5/fpm/php.ini
cp /var/www/vagrant_base/provision/config/etc/php5/fpm/pool.d/www.conf /etc/php5/fpm/pool.d/www.conf

rm -rf /etc/nginx/sites-enabled/default
rm -rf /etc/nginx/sites-available/default
mv /etc/rc2.d/S20nginx /etc/rc2.d/S99nginx
mv /etc/rc3.d/S20nginx /etc/rc3.d/S99nginx
mv /etc/rc4.d/S20nginx /etc/rc4.d/S99nginx
mv /etc/rc5.d/S20nginx /etc/rc5.d/S99nginx



# Restart Nginx for the config to take effect
service php5-fpm restart > /dev/null
service nginx restart > /dev/null

echo "Finished provisioning."